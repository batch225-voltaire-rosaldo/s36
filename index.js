// Setup the dependencies

const express = require("express");
const mongoose = require("mongoose");
const dotenv = require("dotenv").config();

const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3000;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database Connection
// Connect to MongoDB Atlas

mongoose.connect(`mongodb+srv://vrosaldonovulutions:${process.env.PASSWORD}@cluster0.qikdkns.mongodb.net/MRC?retryWrites=true&w=majority`,
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

// Setup notifcation for connection success or failure

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB!"));

app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Server running at port ${port}!`));