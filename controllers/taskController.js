// Controllers contain the functions and business logic of our Express JS. Meaning all the operations it can do will be placed in this file.

const Task = require("../models/task");

module.exports.getAllTasks = () => {

	// The "return" statement returns the result of the 
	// The "then" method is used to wait for the Mongoose
	return Task.find({}).then(result => {
		return result;
	});
};

//*
module.exports.createTask = (requestBody) => {

	// Create a task object based on the Mongoose model "Task"
	let newTask = new Task({

		name : requestBody.name
	})

	return newTask.save().then((task, error) => {

		if (error) {
			console.log(error);

		// If an error is encountered, the "return" statement will prevent any other line or code within the same code block

		// The else statement will no longer be evaluated

		return false;
		
		} else {
			return task
		}
	})

};

module.exports.deleteTask = (taskId) => {

	// The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB
	// The Mongoose method "findByIdAndRemove" method looks for the document using the "_id" field

	return Task.findByIdAndRemove(taskId).then((result, err) => {

		if (err) {

			console.log(err);
			return false;

		} else {
			return result
		}
	})
};

module.exports.updateTask = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false;
		} else {
		
			result.name = newContent.name;

			return result.save().then((updated, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else {
					return updated;
				}
			});
		};
	});
};



// ACTIVITY s36 - January 17, 2023 ===========================

module.exports.updateTaskStatus = (taskId, newContent) => {

	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err)
			return false;
		} else {
		
			result.status = newContent.status;

			return result.save().then((updated, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else {
					return updated;
				}
			});
		};
	});
};

// =========================================================






